package main

import "fmt"
import "strings"

func main() {
	value := func(p string) string {
		return strings.Repeat(p+" ", 3)
	}
	fmt.Println(Exec(value))
}

// Exec function
func Exec(i func(p string) string) string {
	return i("Alan")
}
