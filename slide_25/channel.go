package main

import "fmt"

func main() {
	c := make(chan string)

	go Exec(c)
	c <- "Alan"

	go Exec(c)
	c <- "Steeve"

}

// Exec function
func Exec(c chan string) {
	fmt.Println("Hello " + <-c + "!")
}
