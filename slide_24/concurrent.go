package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Alan")

	concurrent := func() {
		fmt.Println("Pool brake -> started")
		time.Sleep(3 * time.Second)
		fmt.Println("Pool brake -> finished")
	}

	go concurrent()

	time.Sleep(5 * time.Second)
	fmt.Println("Steeeve")
}
