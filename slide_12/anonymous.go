package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(Exec("Xablau"))
}

// Exec function
func Exec(msg string) string {
	return func(s string) string {
		return strings.Repeat(s+" ", 3)
	}(msg)
}
