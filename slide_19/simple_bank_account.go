package main

import (
	"fmt"
)

// BankAccount type struct
type BankAccount struct {
	balance float64
}

func main() {
	account := BankAccount{}

	for i := 0; i < 100000; i++ {
		account.Deposit(10)
		account.Withdraw(account.Balance() / 2)
	}

	fmt.Println(account.Balance())
}

// Balance BankAccount
func (ba *BankAccount) Balance() float64 { return ba.balance }

// Deposit BankAccount
func (ba *BankAccount) Deposit(amount float64) { ba.balance += amount }

// Withdraw BankAccount
func (ba *BankAccount) Withdraw(amount float64) { ba.balance -= amount }
