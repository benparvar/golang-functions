package main

import (
	"errors"
	"fmt"
)

func main() {
	fmt.Println(Exec("Malandragem"))
	fmt.Println(Exec(""))
}

// Exec function
func Exec(msg string) (s string, e error) {
	if msg == "" {
		e = errors.New("Xablau")
	}
	s = msg
	return
}
