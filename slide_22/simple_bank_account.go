package main

import (
	"fmt"
	"sync"
)

// BankAccount type struct
type BankAccount struct {
	balance    float64
	operations chan func(float64) float64
}

func main() {
	var wg sync.WaitGroup

	account := NewBankAccount()

	for i := 0; i < 100000; i++ {
		wg.Add(1)
		go func(acc *BankAccount) {
			acc.Deposit(10)
			acc.Withdraw(acc.Balance() / 2)
			wg.Done()
		}(account)
	}

	go func(acc *BankAccount) {
		account.Loop()
	}(account)
	wg.Wait()

	fmt.Println(account.Balance())
}

// Balance BankAccount
func (ba *BankAccount) Balance() float64 { return ba.balance }

// Deposit BankAccount
func (ba *BankAccount) Deposit(amount float64) {
	ba.operations <- func(oldBalance float64) float64 {
		return oldBalance + amount
	}
}

// Withdraw BankAccount
func (ba *BankAccount) Withdraw(amount float64) {
	ba.operations <- func(oldBalance float64) float64 {
		return oldBalance - amount
	}
}

// Loop BankAccount
func (ba *BankAccount) Loop() {
	for op := range ba.operations {
		ba.balance = op(ba.balance)
	}
}

// NewBankAccount BankAccount
func NewBankAccount() *BankAccount {
	return &BankAccount{
		balance:    0,
		operations: make(chan func(float64) float64),
	}
}
