package main

import "fmt"
import "strings"

func main() {
	value := Exec()
	fmt.Println(value("Alan"))
	fmt.Println(value("Alan"))
	fmt.Println(value("Alan"))
}

// Exec function
func Exec() func(s string) string {
	i := 0
	return func(s string) string {
		i++
		return strings.Repeat(s+" ", i)
	}
}
