package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(Exec())
}

// Exec function
func Exec() string {
	return func() string {
		return strings.Repeat("Steeve ", 3)
	}()
}
