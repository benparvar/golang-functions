package main

import (
	"fmt"
	"strings"
)

// RString type
type RString string

func main() {
	st := RString("Alan")
	fmt.Println(st.Exec())
}

// Exec method
func (s RString) Exec() string {
	str := string(s)
	return strings.Repeat(str+" ", 3)
}
