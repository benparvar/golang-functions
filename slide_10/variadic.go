package main

import (
	"fmt"
)

func main() {
	Exec("Java")
	Exec("Java", "Javascript")
	Exec("Golang", "Javascript", "Kotlin")
}

// Exec function
func Exec(msgs ...string) {
	for _, msg := range msgs {
		fmt.Print(msg + " ")
	}
	fmt.Println()
}
