package main

import "fmt"
import "strings"

func main() {
	value := Exec()
	fmt.Println(value("Alan"))
}

// Exec function
func Exec() func(s string) string {
	return func(s string) string {
		return strings.Repeat(s+" ", 3)
	}
}
