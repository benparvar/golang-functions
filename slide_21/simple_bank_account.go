package main

import (
	"fmt"
	"sync"
)

// BankAccount type struct
type BankAccount struct {
	balance float64
	mu      sync.Mutex
}

func main() {
	var wg sync.WaitGroup

	account := BankAccount{}

	for i := 0; i < 100000; i++ {
		wg.Add(1)
		go func(acc *BankAccount) {
			acc.Deposit(10)
			acc.Withdraw(acc.Balance() / 2)
			wg.Done()
		}(&account)
	}

	wg.Wait()
	fmt.Println(account.Balance())
}

// Balance BankAccount
func (ba *BankAccount) Balance() float64 { return ba.balance }

// Deposit BankAccount
func (ba *BankAccount) Deposit(amount float64) {
	ba.mu.Lock()
	defer ba.mu.Unlock()
	ba.balance += amount
}

// Withdraw BankAccount
func (ba *BankAccount) Withdraw(amount float64) {
	ba.mu.Lock()
	defer ba.mu.Unlock()
	ba.balance -= amount
}
