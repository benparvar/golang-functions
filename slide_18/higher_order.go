package main

import (
	"fmt"
)

// OnResult type function
type OnResult func(number int) string

func main() {
	function := Exec()

	fmt.Println(function(135))
	fmt.Println(function(246))
}

// Exec function
func Exec() OnResult {
	return func(number int) string {
		if number%2 == 0 {
			return "odd"
		}
		return "even"
	}
}
